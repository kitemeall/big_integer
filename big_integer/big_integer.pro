TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    biginteger.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    biginteger.h

