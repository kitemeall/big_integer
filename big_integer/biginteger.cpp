#include "biginteger.h"
#include <stdlib.h>
#include <iostream>
#include <iomanip>
BigInteger::BigInteger(std::string s):
    Head(NULL),
    Tail(NULL),
    negative(false),
    size(0)

{


    if(s.size() > 0)
    {
        if(s[0] == '-')
        {
            negative = true;
            s.erase(s.begin());
        }
        for (int i=(int)s.length(); i>0; i-=9)
            if (i < 9)
                add (atoi (s.substr (0, i).c_str()));
            else
                add (atoi (s.substr (i-9, 9).c_str()));
    }
}

BigInteger::BigInteger (const BigInteger& object):
    Head(NULL),
    Tail(NULL),
    size(0)
{
    negative = object.negative;
    Node* temp = object.Head;
    while(temp != NULL)
    {
        add(temp->x);
        temp = temp->Next;
    }

}

BigInteger::BigInteger(int n ):
    Head(NULL),
    Tail(NULL),
    negative(false),
    size(0)

{
    if(n < 0)
    {
        negative = true;
        n*= -1;
    }

    if(n >= base)
    {
        add(n % base);
        add(n / base);
    }
    else
    {
        add (n);
    }
}

BigInteger::BigInteger():
    BigInteger(0)
{}
//--------------------private-------------------------------------------
void BigInteger::add(int x)
{

    Node *temp=new Node;
    temp->Next=NULL;
    temp->x=x;

    if (Head!=NULL)
    {
        temp->Prev=Tail;
        Tail->Next=temp;
        Tail=temp;
    }
    else
    {
        temp->Prev=NULL;
        Head=Tail=temp;

    }
    size++;
}

void BigInteger::levelUp()
{

    if(Head == NULL)
    {
        add(0);
    }
    else if(Tail->x == 0)
    {
        return;
    }
    else
    {
        Node *temp=new Node;
        temp->Prev=NULL;
        temp->x=0;
        Head->Prev = temp;
        temp->Next = Head;
        Head = temp;
        size ++;
    }
}


void BigInteger::clear()
{
    while (Head != NULL)
    {
        Tail=Head->Next;
        delete Head;
        Head=Tail;
    }
    negative = false;
    size = 0;
}
void BigInteger::pop_back()
{
    Node* temp = Tail;
    Tail = Tail->Prev;
    Tail->Next = NULL;
    delete temp;
    size --;
}

BigInteger::~BigInteger()
{
    //clear();
}

void BigInteger::print(std::ostream &ostream)
{
    if (negative)
        ostream << "-";

    Node *temp = Tail;

    ostream << temp->x;
    temp = temp->Prev;

    while (temp!=NULL)
    {
        ostream << std::setw(9)<<std::setfill('0') << temp->x;
        temp = temp->Prev;
    }

}

bool BigInteger::modulEqual(const BigInteger &object)
{
    bool ans = ( object.size == size);

    Node* p = Head;
    Node* p1 = object.Head;
    if(ans && p != NULL)
    {
        ans = (p->x == p1->x);
        p = p->Next;
        p1 = p1->Next;
    }
    return ans;
}

bool BigInteger::modulBigger(const BigInteger &object)
{
    if(size < object.size)
        return false;
    if( size  > object.size)
        return true;

    Node* p = Tail;
    Node* p1 = object.Tail;

    while(p!= NULL && p->x == p1->x)
    {
        p = p->Prev;
        p1 = p1->Prev;
    }

    if(p != NULL && p->x > p1->x)
        return true;

    return false;
}

bool BigInteger::modulBigEqual(const BigInteger &object)
{
    return (modulBigger(object) || modulEqual(object));
}

bool BigInteger::modulLess(const BigInteger &object)
{
    return (! modulBigEqual(object));
}

bool BigInteger::modulLessEqual(const BigInteger &object)
{
    return (!modulBigger(object));
}


BigInteger BigInteger::modulAdd( const BigInteger &object)
{
    BigInteger newNum;
    newNum.clear();
    int carry = 0;

    Node* p;
    Node* p1;

    if (size > object.size)
    {
        p = Head;
        p1 = object.Head;
    }
    else
    {
        p = object.Head;
        p1 = Head;
    }


    while(p1 != NULL)
    {
        int c = p1->x + p->x + carry;
        carry = c >= base;
        if (carry)
            c -= base;

        newNum.add(c);

        p1 = p1->Next;
        p = p->Next;

    }
    while (p != NULL)
    {
        int c =  p->x + carry;
        carry = c >= base;
        if (carry)
            c -= base;

        newNum.add(c);

        p = p->Next;
    }

    return newNum;
}

bool BigInteger::equal (const BigInteger& object)
{
    bool ans = (object.negative == negative && modulEqual(object));
    return ans;
}

bool BigInteger::bigger(const BigInteger& object)
{
    if(!negative && object.negative)
        return true;

    if(negative && !object.negative)
        return false;

    if(! negative && !object.negative)
        return modulBigger(object);

    else if( negative && object.negative)
        return modulLess(object);

    return false;
}

bool BigInteger::bigEqual(const BigInteger &object)
{
    return (bigger(object) || equal(object));
}

bool BigInteger::less(const BigInteger &object)
{
    return (!bigEqual(object));
}

bool BigInteger::lessEqual(const BigInteger &object)
{
    return (!bigger(object));
}

BigInteger BigInteger::modulSub(BigInteger *object, BigInteger *object1)// object <= this
{
    BigInteger newNum;
    newNum.clear();
    int carry = 0;

    Node* p = object->Head;
    Node* p1 = object1->Head;

    while (p1 != NULL)
    {
        int c = p->x - carry -p1->x;
        carry = c < 0;
        if (carry)
            c += base;
        newNum.add(c);
        p = p->Next;
        p1 = p1->Next;
    }
    while (p != NULL)
    {

        int c = p->x - carry;
        carry = c < 0;
        if (carry)
            c += base;
        newNum.add(c);
        p = p->Next;
    }

    while (newNum.size > 1 && newNum.Tail->x == 0)
        newNum.pop_back();

    return newNum;
}

BigInteger BigInteger::modulMul (const BigInteger &object)const
{


    BigInteger c;

    if(Tail->x == 0 || object.Tail->x == 0)
    {
        return c;
    }
    c.clear();
    for (int i = 0; i < size + object.size; i++)
        c.add(0);


    Node* lessHead = object.Head;
    Node* moreHead = Head;
    Node* cHead = c.Head;
    Node* pC = c.Head;
    Node* pMore = Head; // указатель для числа с большим числом разрядов
    Node* pLess = object.Head;// указатель для числа с меньшим числов разрядов

    if(object.size > size)
    {
        Node* temp = pMore;  //если в числе- аргументе больше разрядов
        pMore = pLess;       //чем в числе-приемнке, то меняем  местами
        pLess = temp;         //указатели

        temp = moreHead;
        moreHead = lessHead;
        lessHead = temp;

    }

    while (pMore != NULL)
    {
        int carry = 0;
        pC = cHead;
        pLess = lessHead;
        while (pLess != NULL || carry)
        {
            long long cur = pC->x + pMore->x * 1ll * ((pLess != NULL) ? pLess->x : 0) + carry;
            pC->x = int (cur % base);
            carry = int (cur / base);

            if(pLess != NULL)
                pLess = pLess->Next;
            pC = pC->Next;
        }
        pMore = pMore->Next;
        cHead = cHead->Next;
    }
    while (c.size > 1 && c.Tail->x== 0)
        c.pop_back();
    return c;
}
//--------------------private-------------------------------------------



BigInteger BigInteger::mul (const BigInteger &object)const
{
    BigInteger ans = modulMul(object);
    if(object.negative != negative)
        ans.negative = true;
    return ans;

}


BigInteger BigInteger:: add( const BigInteger& object)
{
    BigInteger temp;
    temp.clear();

    if (negative == object.negative)
    {
        temp = modulAdd(object);

        if(negative && object.negative)
            temp.negative = true;
    }
    else
    {
        if(modulBigger(object))
        {
            temp = modulSub(this, const_cast<BigInteger*> (&object));
            temp.negative = negative;
        }
        else
        {
            temp = modulSub(const_cast<BigInteger*> (&object), this);
            if(temp > 0)
                temp.negative = object.negative;
        }
    }

    return temp;
}


BigInteger BigInteger::sub(const BigInteger& object)
{
    BigInteger temp(object);
    if(temp.negative == true)
        temp.negative = false;
    else
        temp.negative = true;

    BigInteger temp1 = add(temp);
    //temp.clear();
    return temp1;
}



BigInteger& BigInteger::operator =(const BigInteger& object)
{
    clear();
    negative =object.negative;
    Head = object.Head;
    Tail = object.Tail;
    size = object.size;

    return *this;
}


BigInteger BigInteger::div(const BigInteger &object)
{



    BigInteger res;

    if(modulLess(object))
        return res;

    Node*pR = res.Head;

    BigInteger curValue;

    Node* p = Tail;
    while (p != NULL)
    {
        curValue.levelUp(); // * osn
        curValue.Head->x = p->x;
        // подбираем максимальное число x, такое что b * x <= curValue
        int x = 0;
        int l = 0, r = base;
        while (l <= r)
        {
            int m = (l + r) >> 1;

            BigInteger a(m);
            BigInteger cur =  a * object;
            a.clear();

            if (cur <= curValue)
            {
                x = m;
                l = m+1;
            }
            else
                r = m-1;
            cur.clear();
        }

        pR->x = x;
        res.levelUp();
        if(pR->Prev != NULL)
            pR = pR->Prev;
        // !!!
        BigInteger cc(x);
        cc =  cc.modulMul(object);
        BigInteger newCurV = (curValue - cc);
        curValue = newCurV;

        cc.clear();
        // !!!!

        p = p->Prev;
    }
    // избавляемся от лидирующих нулей
    while (res.size > 1 && res.Tail->x== 0)
        res.pop_back();

    //и одного нуля в конце для не нулевых ответов
    if(res.size >1 && res.Head->x == 0)
    {
        Node* temp = res.Head;
        res.Head = res.Head->Next;
        delete temp;
        res.Head->Prev = NULL;
        res.size--;
    }



    return res;


}

BigInteger BigInteger::mod(const BigInteger &object)
{
    BigInteger k = div(object);

    k = k*object;

    return modulSub(this , &k);
}








