#ifndef BIGINTEGER_H
#define BIGINTEGER_H
#include <string>
#include <iostream>

/*
Цифры числа хранятся в виде двусвязного списка
в порядке уменьшения значимости разрядов. т.е. Head указывает на
самый маленький разряд числа, а Tail на самый большой.
Числа хранятся в системе по модулю Bace.
Для обработки положительных и отрецательных чисел заведен флаг negative, 
который становится true если число отрицательное. Если число = 0 то negative = false
Так как многие операции надо выполнять с отрицительными и положительными числами,
то удобно создать функции выполнения этих операций по модулю, а установу знака
результата вынести в отдельную функцию, которая делает необходимые проверки, вызывает нужную 
модульную операцию и выставляет знак результата.
*/
struct Node
{
    int x;
    Node *Next,*Prev; 
};

class BigInteger
{
private:
    Node *Head,*Tail;
    /*функция добавления к списку числа нового разряда в конец.
    изменяет число, но это изменения не льзя представить как умножение на х.
   т.е. для числа 111111111222222222, хранящегося в виде списка 222222222 - 111111111
   функция add(555) сделает из него число 222222222-111111111-555
   исмользуется с аргументом 0 для расширения числа для будущего поразрядного сознательного заполнения
   или с другим аргументом для построения числа из строки.	  
   */
    void add(int x);	
/*
функйия очистки памяти, занимаемой числом. после ее выполнения и Head и Tail равны NULL
вообще в программе есть куча утечек памяти, но на маленьких данных вроде не заметно=)
*/
    void clear();
/*
Функция удаления разряда из хвоста. Применяется для избавления от незначащих нулей
 т.е. если в какой-то момент число представляется как 111111111 - 0, от вызов pop_back 
сделает из него 111111111 
*/
    void pop_back();
/*Основание системы, по основаню которой хранятся данные*/
    static const int base = 1000*1000*1000;
/* сложение по модулю*/
    BigInteger modulAdd (const BigInteger& object);
/*Вычитание по модулю. Первый аргумент должен быть по модулю больше чем второй*/
    static BigInteger modulSub(BigInteger *object, BigInteger *object1);
/*Умножение по модулю. Логика с сайта http://e-maxx.ru/algo/big_integer*/
    BigInteger modulMul(const BigInteger& object)const;
/*Сравнение по модулю. Реализованы только функции больше
и равно. Остальные выражены через них. Сравнивет объект, у которого
вызывается функция с объектом-аргументом
*/
    bool modulEqual (const BigInteger& object);
    bool modulBigger(const BigInteger& object);
    bool modulBigEqual(const BigInteger& object);
    bool modulLess(const BigInteger& object);
    bool modulLessEqual(const BigInteger& object);
    bool negative;
//текущая длина списка
    int size;


public:
//умножает число на основание системы. Т.Е. увеличивает его в Base раз
    void levelUp();
//конструктор по умолчанию. Слздает число из одного разряда, равное 0
    BigInteger();
//конструктор из строки. Строка должна быть корректной. т.е. либо начинаться с минуса, а дальше цифры
//либо просто содержащей одни цифры. При некорректной строке работа программы будет некорректна
    BigInteger(std::string s);
//конструктор из числа
    BigInteger(int n );
//конструктор копирования. Осуществляет глубокое копирование, т.е. 
//выделяет память и копирует каждый разряд числа 
    BigInteger (const BigInteger& object);
//Деструктор. Память не чистит. Вот одна из утечек памяти! 
//но если его заставить чистить память, будет хуже=) 
    ~BigInteger();
//Вывод числа в поток. Начиная с хвоста выписывает разряд за разрядом
    void print(std::ostream &stream);
//Функйии сравнения. они уже учитывают знаки. 
// вызов того или иного модульного сравнения в зависимосте от знаков
//если одно число положительное, а другое отрицательное, то ответ выдается сразу 
    bool equal (const BigInteger& object);
    bool bigger(const BigInteger& object);
    bool bigEqual(const BigInteger& object);
    bool less(const BigInteger& object);
    bool lessEqual(const BigInteger& object);
//Функции сложения, вычитания. Они тоже в зависимости от знаков вызывают
//модульные операции. Вычитание реализованно через изменением знака операнда 
//и последующее сложение
    BigInteger add(const BigInteger& object);
    BigInteger sub(const BigInteger& object);
//умножение, учитывающее знаки. Вызывает умножение по модулю
    BigInteger mul (const BigInteger &object) const;
//Целочисленное деление. логика с сайта http://cppalgo.blogspot.ru/2010/08/div-mod_29.html
//знаки не учитываются
    BigInteger div (const BigInteger &object);
//взятие остатка. Реализовано через вычитание из числа произведения аргумента 
// и результата div, то есть целой части
    BigInteger mod (const BigInteger & object);
//перегруженные операторы
    bool operator > (const BigInteger& object){return bigger(object);}
    bool operator < (const BigInteger& object){return less(object);}
    bool operator == (const BigInteger& object){return equal(object);}
    bool operator >= (const BigInteger& object){return bigEqual(object);}
    bool operator <= (const BigInteger& object){return lessEqual(object);}
    BigInteger operator +(const BigInteger& object){return add(object);}
    BigInteger operator -(const BigInteger& object){return sub(object);}
    //оператор присваивания. Осуществляет поверхностное присваивание.
    //Т.е. память не выделяется и левая часть начинает указывать на память, 
    // которую выделела правая часть. 	
    BigInteger& operator =(const BigInteger& object);
    BigInteger operator * (const BigInteger& object)const{return mul(object);}
    BigInteger operator / (const BigInteger & object){return div(object);}
    BigInteger operator % (const BigInteger& object) { return mod(object);}

};

//перегруженные операторы вывода в поток.
inline std::ostream & operator <<(std::ostream & ostream, BigInteger  & n)
{
    n.print(ostream);
    return ostream;
}
inline std::ostream & operator <<(std::ostream & ostream, BigInteger   n)
{
    n.print(ostream);
    return ostream;
}

#endif // BIGINTEGER_H
